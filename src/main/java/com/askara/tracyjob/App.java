package com.askara.tracyjob;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class App {
	private static SessionFactory sessionFactory = null;  
	private static ServiceRegistry serviceRegistry = null;
	private static XSSFWorkbook workbook = null;
	private static Session session = null;
	private static String baseName = null;
	  
 	private static SessionFactory configureSessionFactory(String fileName) throws HibernateException {  
		
		DateFormat dateFormat = new SimpleDateFormat("_dd.MM.yyyy_HH.mm");
		Date date = new Date();
		
	    Configuration configuration = new Configuration();  
	    configuration.configure(); 
//	    configuration.setProperty("hibernate.connection.url", "jdbc:sqlite:aa.tracyjob");
	    configuration.setProperty("hibernate.connection.url", "jdbc:sqlite:" + fileName + dateFormat.format(date)+".tracyjob");
	    
	    Properties properties = configuration.getProperties();
	    
		serviceRegistry = new ServiceRegistryBuilder().applySettings(properties).buildServiceRegistry();          
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);  
	    
	    return sessionFactory;  
	}
	
	private static void configureSessionFactory2() throws HibernateException {  
        Configuration configuration = new Configuration();  
        configuration.configure();  
         
        Properties properties = configuration.getProperties();
         
        serviceRegistry = new ServiceRegistryBuilder().applySettings(properties).buildServiceRegistry();          
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);  
        
        
    }
	
	
	public static void main(String[] args) {
		
		String fileArg = args[0];
		File xls = new File(fileArg);
		String absolutePath = xls.getAbsolutePath();
    	String filePath = absolutePath.substring(0,absolutePath.lastIndexOf(File.separator));
    	System.setProperty("user.dir", filePath);
		
    	// Configure the session factory
		configureSessionFactory(xls.getName());
		//configureSessionFactory();
		
		Transaction tx=null;
		
		try {
			FileInputStream file = new FileInputStream(xls);
            workbook = new XSSFWorkbook(file);
			
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			tabCodes();
			tabControlPoints();
			tabCoordinateSystems();
			tabFeatureValues();
			tabLocalizations();
			tabRTKBases();
			tabRepresentations();
			tabSettings();
			tabSurveyInfos();
			tabSurveyPoints();
			tabTransformationLists();
			tabVersions();
			tx.commit();
			
		} catch (Exception ex) {
			ex.printStackTrace();
			tx.rollback();
		} finally{
			if(session != null) {
				session.close();
			}
		}
	}
	
	private static void tabCodes(){
		XSSFSheet sheet = workbook.getSheet("Codes");
		 
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        while (rowIterator.hasNext()) 
        {
            Row row = rowIterator.next();
            
            String name	= row.getCell(0).toString();
            String description = null; 				//row.getCell(1).toString();
            int representation = 28; 				//(int) row.getCell(2).getNumericCellValue();
            int usecount = 5; 						//(int) row.getCell(3).getNumericCellValue(); 
            java.sql.Timestamp lastusetime = new Timestamp(row.getCell(1).getDateCellValue().getTime());
            Integer codegroup = null; 				//(int) row.getCell(5).getNumericCellValue();
            int codenumber = 1; 					//row.getCell(6).toString();
            int polyline = 0; 						//(int) row.getCell(7).getNumericCellValue();
            int polyline3D = 0; 					//(int) row.getCell(8).getNumericCellValue();
            int layerid = 0;						//(int) row.getCell(9).getNumericCellValue();

            Query  query = session.createSQLQuery("INSERT INTO tab_Codes VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
    				.setParameter(0, name).setParameter(1, description)
    				.setParameter(2, representation).setParameter(3, usecount)
    				.setParameter(4, lastusetime).setParameter(5,codegroup)
    				.setParameter(6, codenumber).setParameter(7, polyline)
    				.setParameter(8, polyline3D).setParameter(9, layerid);
            
            query.executeUpdate();
        }
	}

	private static void tabControlPoints(){
		XSSFSheet sheet = workbook.getSheet("ControlPoints");
		 
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        int id = 1;
        while (rowIterator.hasNext()) 
        {
            Row row = rowIterator.next();
                        
            String name = row.getCell(0).toString();
            String comment = null; 			//row.getCell(0).toString();
            String catalog = "catalog";		//row.getCell(0).toString();
            String coordinatesystem = "CSP_TURKEY_ITRF$ZNA_Z1_"+ row.getCell(1).toString() + "DEG";
            String heightsystem = "$";	 	//row.getCell(2).toString();
            String n = row.getCell(2).toString();
            String e = row.getCell(3).toString();
            String h = row.getCell(4).toString();
            int qualitytype = 127;			//(int) row.getCell(6).getNumericCellValue();
            double sigmah = 0d;				//row.getCell(7).getNumericCellValue();
            double sigmav = 0d;				//row.getCell(8).getNumericCellValue();
            String userdefined =  null;		//row.getCell(9).toString();
            String code = null;				//row.getCell(10).toString();
            String speccode = null;			//row.getCell(11).toString();
            int representation = 0;			//(int) row.getCell(12).getNumericCellValue();

            session.createSQLQuery("INSERT INTO tab_ControlPoints VALUES("+ id +", ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
    				.setParameter(0, name).setParameter(1, comment)
            		.setParameter(2, catalog).setParameter(3, coordinatesystem)
    				.setParameter(4, heightsystem).setParameter(5, n)
    				.setParameter(6, e).setParameter(7,h)
    				.setParameter(8, qualitytype).setParameter(9, sigmah)
    				.setParameter(10, sigmav).setParameter(11, userdefined)
		            .setParameter(12, code).setParameter(13, speccode)
		            .setParameter(14, representation).executeUpdate();
            id++;
        }
	}
	
	private static void tabCoordinateSystems(){
		
		session.createSQLQuery("INSERT INTO tab_CoordinateSystems VALUES('CSG_WGS_84$','CSE_WGS_84$','WGS 84',1,'',0,'GRP_WORLD','','',NULL,'','ELL_WGS_84',6378137,298.257223563,'','',0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_CoordinateSystems VALUES('CSL_LOCAL$','','Local',3,'',0,'GRP_WORLD','','',NULL,'','',0,1,'','',0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL)").executeUpdate();
		
		XSSFSheet sheet = workbook.getSheet("CoordinateSystems");
		 
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        while (rowIterator.hasNext()) 
        {
            Row row = rowIterator.next();
            
            String csid	= "CSP_TURKEY_ITRF$ZNA_Z1_"+ row.getCell(0).toString() +"DEG";
            String basecsid	= "CSG_ETRS89$";				//row.getCell(1).toString();
            String name	= row.getCell(1).toString();
            int type = 2;									//(int) row.getCell(3).getNumericCellValue();
            String axistype	= null;							//row.getCell(4).toString();
            int editable = 0;								//(int) row.getCell(5).getNumericCellValue();
            String groupid = "GRP_TURKEY_ITRF";				//row.getCell(6).toString();
            String areaid = null;							//row.getCell(7).toString();
            String remark = "Turkey system on ETRS instead of ED50";	//row.getCell(8).toString();
            String informationsource = null;				//row.getCell(9).toString();
            String primemeridian = null;					//row.getCell(10).toString();
            String ellipsoid = "ELL_GRS_1980";				//row.getCell(11).toString();
            double a = 6378137.0d;							//row.getCell(12).getNumericCellValue();
            double f = 0.0033528107d;						//row.getCell(13).getNumericCellValue();
            String projection = "PRJ_Z1_"+ row.getCell(2).toString() +"DEG";
            String projectionmodel = "MPR_TRANSVERSE_MERCATOR";		//row.getCell(15).toString();
            double p1 = 500000.0d;							//row.getCell(16).getNumericCellValue();
            double p2 = 0.0d;								//row.getCell(17).getNumericCellValue();
            double p3 = 0.0d;								//row.getCell(18).getNumericCellValue();
            double p4 = 0.471238898038d;					//row.getCell(19).getNumericCellValue();
            double p5 = 1.0d;								//row.getCell(20).getNumericCellValue();
            double p6 = 0.0d;								//row.getCell(21).getNumericCellValue();
            double p7 = 0.0d;								//row.getCell(22).getNumericCellValue();
            double p8 = 0.0d;								//row.getCell(23).getNumericCellValue();
            double p9 = 0.0d;								//row.getCell(24).getNumericCellValue();
            double p10 = 0.0d;								//row.getCell(25).getNumericCellValue();
            Double p11 = null;								//row.getCell(26).toString();
    		Double p12 = null;								//row.getCell(27).toString();
            Double p13 = null;								//row.getCell(28).toString();
            Double p14 = null;								//row.getCell(29).toString();

            session.createSQLQuery(
    				"INSERT INTO tab_CoordinateSystems VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
    				.setParameter(0, csid).setParameter(1, basecsid)
    				.setParameter(2, name).setParameter(3, type)
    				.setParameter(4, axistype).setParameter(5,editable)
    				.setParameter(6, groupid).setParameter(7, areaid)
    				.setParameter(8, remark).setParameter(9, informationsource)
    				.setParameter(10, primemeridian).setParameter(11, ellipsoid)
    				.setParameter(12, a).setParameter(13, f)
    				.setParameter(14, projection).setParameter(15,projectionmodel)
    				.setParameter(16, p1).setParameter(17, p2)
    				.setParameter(18, p3).setParameter(19, p4)
    				.setParameter(20, p5).setParameter(21, p6)
    				.setParameter(22, p7).setParameter(23, p8)
    				.setParameter(24, p9).setParameter(25,p10)
    				.setParameter(26, p11).setParameter(27, p12)
    				.setParameter(28, p13).setParameter(29, p14).executeUpdate();
        }
	}

	private static void tabFeatureValues(){
		
		XSSFSheet sheet = workbook.getSheet("ServeyPoints");
		 
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        int id = 1;
        String code	= "";
        while (rowIterator.hasNext()) 
        {
            Row row = rowIterator.next();
            
            int type = 1;					//(int) row.getCell(0).getNumericCellValue(); 
            code	= "";
            if(row.getCell(1) !=null)
            	code = row.getCell(1).toString();
            String feature = "";			//row.getCell(2).toString();
            String value = "";				//row.getCell(3).toString();

            session.createSQLQuery("INSERT INTO tab_FeatureValues VALUES("+id+",?, ?, ?, ?)")
    				.setParameter(0, type).setParameter(1, code)
    				.setParameter(2, feature).setParameter(3, value).executeUpdate();
            id++;
        }
	}

	private static void tabLocalizations(){
        session.createSQLQuery("INSERT INTO tab_Localizations VALUES(1,1,'CSG_WGS_84$',NULL,'CSL_LOCAL$',NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,'$','','','')").executeUpdate();
	}

	private static void tabRTKBases(){
		
		XSSFSheet sheet = workbook.getSheet("RTKBases");
		 
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        int id = 1;
        while (rowIterator.hasNext()) 
        {
            Row row = rowIterator.next();
            
            int baseid = 1;								//(int) row.getCell(0).getNumericCellValue(); 
            String name	= row.getCell(0).toString();
            String coordinatesystem	= "CSG_WGS_84$";	//row.getCell(2).toString();
            String heightsystem	= "$";					//row.getCell(3).toString();
            String Lat = row.getCell(1).toString();
            String Lon = row.getCell(2).toString();
            String Hgt = row.getCell(3).toString();
            int pointid = 0;							//(int) row.getCell(7).getNumericCellValue(); 
            String pointname = "autopos_1";				//row.getCell(8).toString();
            String pointcoordinatesystem = "CSG_WGS_84$";	//row.getCell(9).toString();
            String pointheightsystem = "$";					//row.getCell(10).toString();
            double pointN  = 0.0d;							//row.getCell(11).getNumericCellValue(); 
            double pointE  = 0.0d;							//row.getCell(12).getNumericCellValue(); 
            double pointH  = 0.0d;							//row.getCell(13).getNumericCellValue(); 
            String AntennaModel = "JAV_TRIUMPH-1   NONE";	//row.getCell(14).toString();
            String AntennaHeight  = row.getCell(4).toString();
            int AntennaHeightType = 1;						//(int) row.getCell(16).getNumericCellValue(); 
            String ReceiverStyle = null;					//row.getCell(17).toString();
            String representation = null;					//row.getCell(18).toString();
            int surveyinfo = 1;								//(int) row.getCell(19).getNumericCellValue(); 
            
            baseName = name;
            
            session.createSQLQuery(
    				"INSERT INTO tab_RTKBases VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)").setParameter(0, id)
    				.setParameter(1, baseid).setParameter(2, name)
    				.setParameter(3, coordinatesystem).setParameter(4, heightsystem)
    				.setParameter(5, Lat).setParameter(6,Lon)
    				.setParameter(7, Hgt).setParameter(8, pointid)
    				.setParameter(9, pointname).setParameter(10, pointcoordinatesystem)
    				.setParameter(11, pointheightsystem).setParameter(12, pointN)
    				.setParameter(13, pointE).setParameter(14, pointH)
    				.setParameter(15, AntennaModel).setParameter(16,AntennaHeight)
    				.setParameter(17, AntennaHeightType).setParameter(18, ReceiverStyle)
    				.setParameter(19, representation).setParameter(20, surveyinfo).executeUpdate();
            id++;
        }
        
        session.createSQLQuery("INSERT INTO tab_RTKBases VALUES("+ id +",1,'RTCM3Net(212.156.70.42:2101)','CSG_WGS_84$','$',38.497004789012,27.705707494433,126.510256998241,0,'','$','$',0,0,0,'000',1.4,0,NULL,NULL,2)").executeUpdate();
		
	}
	
	private static void tabRepresentations(){
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(1,1,'C',65280,2,1,'',1,65280,5)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(2,1,'T',33023,2,1,'',1,33023,5)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(3,1,'S',16711680,2,1,'',1,16711680,5)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(5,1,'T',255,2,1,'',1,255,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(6,1,'C',0,1,1,'',1,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(7,1,'C',0,1,1,'',1,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(8,1,'C',0,1,1,'',1,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(9,1,'C',0,1,1,'',1,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(13,1,'C',0,1,1,'',1,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(15,1,'C',33023,2,5,'',1,33023,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(16,1,'C',8421504,2,0,'',1,8421504,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(17,1,'C',255,1,1,'',1,255,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(18,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(19,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(20,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(21,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(22,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(23,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(24,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(25,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(26,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(27,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(28,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(29,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(30,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(31,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(32,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(33,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(34,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(35,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(36,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(37,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Representations VALUES(38,1,'C',65280,2,3,'',2,0,0)").executeUpdate();
	}

	private static void tabSettings(){
		
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('CS_MEASURE_UNITS','0')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('CS_MEASURE_UNITS_FEET_FORMAT','0')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('CS_MEASURE_UNITS_LATLON_FORMAT','0')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('CS_COORDINATE_ORDER','0')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('CS_INVERTED_LOCAL_AXIS','0')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('CS_Localization','1')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('Catalog','Default Catalog')").executeUpdate();
		//session.createSQLQuery("INSERT INTO tab_Settings VALUES('Job_Name','YUSUF')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('Job_Description','')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('Job_Surveyer','')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('Job_Agency','')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('Job_UserInfo','')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('CS_GEOID','$')").executeUpdate();
		//session.createSQLQuery("INSERT INTO tab_Settings VALUES('CS_CWS','CSP_TURKEY_ITRF$ZNA_Z1_27DEG')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('CS_CVS','$')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('ReceiverStyle_Style','')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('SurveyStyle_Style','')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('EquipmentInfo_ReceiverName','TRIUMPH1')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('EquipmentInfo_ReceiverSN','03342')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('EquipmentInfo_AntennaType','JAV_TRIUMPH-1   NONE')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('EquipmentInfo_ControllerName','Javad Victor')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('EquipmentInfo_ControllerSN','95800')").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Settings VALUES('CurCode','YOL')").executeUpdate();
		
		XSSFSheet sheet = workbook.getSheet("Settings");
		 
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        while (rowIterator.hasNext()) 
        {
            Row row = rowIterator.next();
            
            String key	= row.getCell(0).toString();
            String value = row.getCell(1).toString();
            
            if("CS_CWS".equalsIgnoreCase(key))
            	value = "CSP_TURKEY_ITRF$ZNA_Z1_" + value + "DEG";

            session.createSQLQuery("INSERT INTO tab_Settings VALUES(?, ?)")
    				.setParameter(0, key).setParameter(1, value).executeUpdate();
        }
	}

	private static void tabSurveyInfos2(){
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(1,1,5,5,0,2.14622,0,0,0,0,0,0,873,558483000,181948628,181948628,0,'JAV_TRIUMPH-1   NONE',1.365,1,1.455486,0,1.897406,1.003052,0,0,0,0.182)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(2,0,0,0,0,0,0,0,0,0,0,0,0,0,182748776,182748776,0,'000',1.4,0,0,0,0,0,0,0,0,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(3,4,4,4,0,2.538329,0.000166,0.000069,0.000126,0.000081,0.000071,0.00025,873,559285000,182747470,182750028,3,'JAV_TRIUMPH-1   NONE',2.4,0,1.97112,3.213787,2.251879,1.17139,0,0,0,0.182)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(4,4,5,4,0,2.061169,0.000434,0.000033,0.000076,0.000045,0.00002,0.00025,873,559294000,182756797,182759029,3,'JAV_TRIUMPH-1   NONE',2.4,0,1.484798,2.540284,1.738134,1.107839,0,0,0,0.182)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(5,4,4,4,0,2.915758,0.000438,0.000055,0.000139,0.000051,0.000013,0.000285,873,559303000,182765579,182768008,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.139878,3.616728,2.411478,1.639029,0,0,0,0.182)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(6,4,4,3,0,3.612684,0.000429,0.000078,0.000043,0.000069,-0.000047,0.000736,873,559311000,182773802,182776008,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.310262,4.288216,2.463675,2.642308,0,0,0,0.182)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(7,4,4,3,0,3.611556,0.000381,0.000078,0.000043,0.000147,-0.000047,0.001294,873,559319000,182781729,182783959,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.311051,4.287691,2.463391,2.64103,0,0,0,0.182)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(8,4,5,2,0,3.390082,0.000828,0.000047,0.000025,0.000221,-0.000081,0.000852,873,559342000,182803490,182806936,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.158293,4.018816,2.109888,2.653493,0,0,0,0.182)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(9,4,5,2,0,3.385339,0.000323,0.000046,0.000025,0.000166,-0.00008,0.000693,873,559354000,182816778,182818740,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.157693,4.014494,2.110352,2.647062,0,0,0,0.182)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(10,4,4,4,0,3.21548,0.000525,0.000109,0.000221,0.000313,0.000133,0.004118,873,559883000,183345353,183347782,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.553061,4.10578,2.921841,1.342444,0,0,0,0.182)").executeUpdate();
	}
	
	private static void tabSurveyInfos() throws ParseException{
		
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(1,1,5,5,0,2.14622,0,0,0,0,0,0,873,558483000,181948628,181948628,0,'JAV_TRIUMPH-1   NONE',1.365,1,1.455486,0,1.897406,1.003052,0,0,0,0.182)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(2,0,0,0,0,0,0,0,0,0,0,0,0,0,182748776,182748776,0,'000',1.4,0,0,0,0,0,0,0,0,0)").executeUpdate();
		
		
		XSSFSheet sheet = workbook.getSheet("ServeyPoints");
		 
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        SimpleDateFormat dateFormat = 	new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        java.sql.Date t2 = new java.sql.Date(dateFormat.parse("2016.05.15 02:59:45").getTime());
        int id = 3;
        double baseTime = 182747470d;
                
        
        while (rowIterator.hasNext()) 
        {
            Row row = rowIterator.next();
            
            //SolutionType	ngps	nglo	ngal	PDOP	C_XX	C_XY	C_XZ	C_YY	C_YZ	C_ZZ	gpsweek	gpsseconds	TimeStart	TimeEnd	
            //epochcounted	AntennaModel	AntennaHeight	AntennaHeightType	TDOP	GDOP	VDOP	HDOP	AntennaOffsetN	AntennaOffsetE	
            //AntennaOffsetH	AntennaVerticalOffset
            
            
            
            int SolutionType = 4; 									//(int) row.getCell(0).getNumericCellValue();
            int ngps = ThreadLocalRandom.current().nextInt(4, 6);	//(int) row.getCell(1).getNumericCellValue();
            int nglo = ThreadLocalRandom.current().nextInt(2, ngps+1);	//(int) row.getCell(2).getNumericCellValue();
            int ngal = 0;												//(int) row.getCell(3).getNumericCellValue();
            String PDOP = row.getCell(7).toString();
            double C_XX = ThreadLocalRandom.current().nextDouble(0.0001000d, 0.0008000d); 
            double C_XY = ThreadLocalRandom.current().nextDouble(0.0000400d, 0.0001000d);
            double C_XZ = ThreadLocalRandom.current().nextDouble(0.0000200d, 0.0001000d);
            double C_YY = ThreadLocalRandom.current().nextDouble(0.0000200d, 0.0001000d);
            double C_YZ = ThreadLocalRandom.current().nextDouble(0.0000200d, 0.0001000d);
            double C_ZZ = ThreadLocalRandom.current().nextDouble(0.0000200d, 0.0001000d);
            int gpsweek = 873; 
            long gpsseconds = row.getCell(5).getDateCellValue().getTime() - t2.getTime();
            baseTime = getIncreaseVal(baseTime, 6000d,10000d);
            double TimeStart = baseTime;
            baseTime = getIncreaseVal(baseTime, 2000d,3000d);
            double TimeEnd = baseTime;
            
            //String tmp = row.getCell(6).toString();
			int epochcounted = 0;
			try{
				epochcounted = (int) row.getCell(6).getNumericCellValue();
			} catch(Exception e) {
				epochcounted = Integer.parseInt(row.getCell(6).toString());
			}
		
            String AntennaModel = "JAV_TRIUMPH-1   NONE"; //row.getCell(16).toString();
            String AntennaHeight = row.getCell(8).toString();
            int AntennaHeightType = "D��ey".equalsIgnoreCase(row.getCell(9).toString()) ? 0:1;
            double TDOP = getIncreaseVal(1.8d, 0d, 0.8d);
            double GDOP = getIncreaseVal(2.5d, 0d, 2d);
            double VDOP = getIncreaseVal(1.8d, 0d, 1d);
            double HDOP = getIncreaseVal(1.1d, 0d, 1.5d);
            double AntennaOffsetN = 0d;
            double AntennaOffsetE = 0d;
            double AntennaOffsetH = 0d;
            double AntennaVerticalOffset = 0.182d;
            
            

            session.createSQLQuery("INSERT INTO tab_SurveyInfos VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
    				.setParameter(0, id++)
            		.setParameter(1, SolutionType).setParameter(2, ngps)
    				.setParameter(3, nglo).setParameter(4, ngal)
    				.setParameter(5, PDOP).setParameter(6,C_XX)
    				.setParameter(7, C_XY).setParameter(8, C_XZ)
    				.setParameter(9, C_YY).setParameter(10, C_YZ)
    				.setParameter(11, C_ZZ).setParameter(12, gpsweek)
    				.setParameter(13, gpsseconds).setParameter(14, TimeStart)
    				.setParameter(15, TimeEnd).setParameter(16,epochcounted)
    				.setParameter(17, AntennaModel).setParameter(18, AntennaHeight)
    				.setParameter(19, AntennaHeightType).setParameter(20, TDOP)
		            .setParameter(21, GDOP).setParameter(22, VDOP)
    				.setParameter(23, HDOP).setParameter(24, AntennaOffsetN)
    				.setParameter(25, AntennaOffsetE).setParameter(26,AntennaOffsetH)
    				.setParameter(27, AntennaVerticalOffset)
		            .executeUpdate();
        }
	}
	
	private static void tabSurveyPoints(){
		XSSFSheet sheet = workbook.getSheet("ServeyPoints");
		 
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        int info = 3;
        int id = 0;
        String code = "";
        while (rowIterator.hasNext()) 
        {
            Row row = rowIterator.next();
            
            //name	comment	code	image	dataset	coordinatesystem	heightsystem	coordinatetype	lat	lon	hgt	sigmah	
            //sigmav	rawdata	base	surveyinfo	speccode	representation
            
            String name = row.getCell(0).toString();
            String comment = null;				//row.getCell(1).toString();
            code = "";
            if(row.getCell(1)!=null)
            	code = row.getCell(1).toString();
            String image = null;				//row.getCell(3).toString();
            String dataset = null;				//row.getCell(4).toString();
            String coordinatesystem = "CSG_WGS_84$";	//row.getCell(5).toString();
            String heightsystem = "$";					//row.getCell(6).toString();
            String coordinatetype = null;				//row.getCell(7).toString();
            String lat = row.getCell(2).toString();
            String lon = row.getCell(3).toString();
            String hgt = row.getCell(4).toString();
            double sigmah = 0.013815d;					//row.getCell(11).getNumericCellValue();
            double sigmav = 0.019663d;					//row.getCell(12).getNumericCellValue();
            String rawdata = null;						//row.getCell(13).toString();
            int base = 1;								//(int) row.getCell(14).getNumericCellValue();
            int surveyinfo = info++;					//(int) row.getCell(15).getNumericCellValue();
            String speccode = null;						//row.getCell(16).toString();
            int representation = 0;						//(int) row.getCell(17).getNumericCellValue();
            
            session.createSQLQuery("INSERT INTO tab_SurveyPoints VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
            		.setParameter(0, ++id)
    				.setParameter(1, name).setParameter(2, comment)
    				.setParameter(3, code).setParameter(4, image)
    				.setParameter(5, dataset).setParameter(6,coordinatesystem)
    				.setParameter(7, heightsystem).setParameter(8, coordinatetype)
    				.setParameter(9, lat).setParameter(10, lon)
		            .setParameter(11, hgt).setParameter(12, sigmah)
    				.setParameter(13, sigmav).setParameter(14, rawdata)
    				.setParameter(15, base).setParameter(16,surveyinfo)
    				.setParameter(17, speccode).setParameter(18, representation).executeUpdate();
            
        }
	}
	
	private static void tabTransformationLists(){
		
		session.createSQLQuery("INSERT INTO tab_TransformationLists VALUES(1,'CSG_WGS_84$','$','CSL_LOCAL$','$','<TRANSFORM_LIST SOURCE_COORDINATE_SYSTEM_NAME=\"WGS 84\" SOURCE_COORDINATE_SYSTEM_ID=\"CSG_WGS_84\" SOURCE_COORDINATE_SYSTEM_ZN=\"\" SOURCE_VERTICAL_SYSTEM_ID=\"\" SOURCE_VERTICAL_SYSTEM_ZN=\"\" TARGET_COORDINATE_SYSTEM_NAME=\"Local engineering grid\" TARGET_COORDINATE_SYSTEM_ID=\"CSL_LOCAL\" TARGET_COORDINATE_SYSTEM_ZN=\"\" TARGET_VERTICAL_SYSTEM_ID=\"\" TARGET_VERTICAL_SYSTEM_ZN=\"\" LANGUAGE_ID=\"en-US\"> <TRANSFORM NAME=\"WGS to Local\" selected=\"true\"> <MDL_GEODETIC_LOCALIZATION  PAR_LATITUDE_OFFSET=\"0.000000000000000\" PAR_LONGITUDE_OFFSET=\"0.000000000000000\" PAR_ROTATION_ANGLE_OF_SOURCE_COORDINATE_REFERENCE_SYSTEM_AXES=\"0.000000000000000\" PAR_SCALE_DIFFERENCE=\"0.000000000000000\" PAR_ORDINATE_1_OF_EVALUATION_POINT=\"0.000000\" PAR_ORDINATE_2_OF_EVALUATION_POINT=\"0.000000\" PAR_VERTICAL_OFFSET=\"0.000000\" PAR_INCLINATION_IN_NORTHING=\"0.000000000000000\" PAR_INCLINATION_IN_EASTING=\"0.000000000000000\" PAR_SEMI_MAJOR_AXIS_SOURCE=\"6378137.0\" PAR_FLATTENING_SOURCE=\"0.0033528106647474807198455286185206\" PAR_PRIME_MERIDIAN_GREENWICH_LONGITUDE_SOURCE=\"0\" PAR_SEMI_MAJOR_AXIS_TARGET=\"-1\" PAR_FLATTENING_TARGET=\"-1\" PAR_PRIME_MERIDIAN_GREENWICH_LONGITUDE_TARGET=\"0\" forward=\"true\" /> </TRANSFORM> </TRANSFORM_LIST>','')").executeUpdate();
		
		XSSFSheet sheet = workbook.getSheet("TransformationLists");
		 
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        int id = 1;
        while (rowIterator.hasNext()) 
        {
            Row row = rowIterator.next();
            
            //cs1	vs1	cs2	vs2	xml	selected
            
            String cs1 = "CSG_WGS_84$";		//row.getCell(0).toString();
            String vs1 = "$";				//row.getCell(1).toString();
            String cs2 = row.getCell(0).toString();
            String vs2 = "$"; 				//row.getCell(3).toString();
            String xml = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?> <TRANSFORM_LIST  	SOURCE_COORDINATE_SYSTEM_NAME=\"WGS84(ITRF2008)\"  	SOURCE_COORDINATE_SYSTEM_ID=\"CSG_WGS_84\"  	SOURCE_COORDINATE_SYSTEM_ZN=\"\"  	SOURCE_VERTICAL_SYSTEM_ID=\"\"  	SOURCE_VERTICAL_SYSTEM_ZN=\"\"  	TARGET_COORDINATE_SYSTEM_NAME=\"TR-ITRF / 27-3-ITRF\"  	TARGET_COORDINATE_SYSTEM_ID=\"CSP_TURKEY_ITRF\"  	TARGET_COORDINATE_SYSTEM_ZN=\"ZNA_Z1_27DEG\"  	TARGET_VERTICAL_SYSTEM_ID=\"\"  	TARGET_VERTICAL_SYSTEM_ZN=\"\"  	SOUTH_BOUND_LAT=\"0.63704517697793\"  	NORTH_BOUND_LAT=\"0.734783615089613\"  	WEST_BOUND_LON=\"0.446629755585349\"  	EAST_BOUND_LON=\"0.497418836818384\"  	EPOCH=\"2016.38524590164\"  	LANGUAGE_ID=\"en-US\"  > 	<TRANSFORM NAME=\"ETRS89 to WGS 84 (1) / EPSG-eur / 3-degree TM zone 1\"  	SOUTH_BOUND_LAT=\"0.602138591938044\"  	NORTH_BOUND_LAT=\"1.24005643354197\"  	WEST_BOUND_LON=\"0.445058959258554\"  	EAST_BOUND_LON=\"0.497418836818384\"  	selected=\"true\"> 		<MDL_POSITION_VECTOR_7_PARAM_TRANSFORMATION PAR_FLATTENING_SOURCE=\"0.00335281068118232\" PAR_FLATTENING_TARGET=\"0.00335281066474748\" PAR_PRIME_MERIDIAN_GREENWICH_LONGITUDE_SOURCE=\"0\" PAR_PRIME_MERIDIAN_GREENWICH_LONGITUDE_TARGET=\"0\" PAR_SCALE_DIFFERENCE=\"0\" PAR_SEMI_MAJOR_AXIS_SOURCE=\"6378137\" PAR_SEMI_MAJOR_AXIS_TARGET=\"6378137\" PAR_X_AXIS_ROTATION=\"0\" PAR_X_AXIS_TRANSLATION=\"0\" PAR_Y_AXIS_ROTATION=\"0\" PAR_Y_AXIS_TRANSLATION=\"0\" PAR_Z_AXIS_ROTATION=\"0\" PAR_Z_AXIS_TRANSLATION=\"0\" forward=\"false\"/> 		<MPR_TRANSVERSE_MERCATOR PAR_FALSE_EASTING=\"500000\" PAR_FALSE_NORTHING=\"0\" PAR_FLATTENING=\"0.00335281068118232\" PAR_LATITUDE_OF_NATURAL_ORIGIN=\"0\" PAR_LONGITUDE_OF_NATURAL_ORIGIN=\"0.47123889803846\" PAR_PRIME_MERIDIAN_GREENWICH_LONGITUDE=\"0\" PAR_SCALE_FACTOR_AT_NATURAL_ORIGIN=\"1\" PAR_SEMI_MAJOR_AXIS=\"6378137\" forward=\"true\"/> 	</TRANSFORM> </TRANSFORM_LIST> ";				//row.getCell(4).toString();
            String selected = null;			//row.getCell(5).toString();
            
            xml = xml.replace("27", cs2);
            cs2 = "CSP_TURKEY_ITRF$ZNA_Z1_" + cs2 + "DEG";
            session.createSQLQuery("INSERT INTO tab_TransformationLists VALUES(?, ?, ?, ?, ?, ?, ?)")
            		.setParameter(0, ++id)
    				.setParameter(1, cs1).setParameter(2, vs1)
    				.setParameter(3, cs2).setParameter(4, vs2)
    				.setParameter(5, xml).setParameter(6,selected).executeUpdate();
        }
	}
	
	private static void tabVersions(){
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(1,'tab_Versions',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(2,'tab_Settings',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(3,'tab_ControlPoints',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(4,'tab_SurveyPoints',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(5,'tab_DesignPoints',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(6,'tab_PointsToSurvey',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(7,'tab_BackgroundMapPoints',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(8,'tab_BackgroundImages',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(9,'tab_BackgroundLayers',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(10,'tab_Codes',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(11,'tab_Features',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(12,'tab_Domains',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(13,'tab_ValueLists',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(14,'tab_FeatureValues',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(15,'tab_CodeGroups',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(16,'tab_CoordinateSystems',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(17,'tab_Localizations',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(18,'tab_IdenticalPoints',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(19,'tab_IsolinePoints',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(20,'tab_Datums',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(21,'tab_TransformationLists',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(22,'tab_PointLists',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(23,'tab_RTKBases',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(24,'tab_SurveyInfos',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(25,'tab_RawDatas',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(26,'tab_Stakeouts',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(27,'tab_Antennas',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(28,'tab_Selections',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(29,'tab_JobHistory',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(30,'tab_Representations',2,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(31,'tab_ComplexObjectParts',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(32,'tab_ComplexObjects',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(33,'tab_RoadsEx',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(34,'tab_RoadStartPointsEx',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(35,'tab_RoadHAlignLinesEx',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(36,'tab_RoadHAlignArcsEx',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(37,'tab_RoadHAlignSpiralsEx',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(38,'tab_RoadVAlignGradesEx',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(39,'tab_RoadVAlignParabolsEx',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(40,'tab_RoadXSections',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(41,'tab_RoadXSectionTemplates',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(42,'tab_RoadXSectionSegments',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(43,'tab_MediaFiles',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(44,'tab_TotalStationPoints',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(45,'tab_TotalStationMeasurements',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(46,'tab_CoordinatesCWS',1,0)").executeUpdate();
		session.createSQLQuery("INSERT INTO tab_Versions VALUES(47,'DataBase',50,0)").executeUpdate();
        }
	
	private static double getIncreaseVal(double d, double min, double max){
		return d + ThreadLocalRandom.current().nextDouble(min, max);
	}
	
	/*

	--INSERT INTO tab_Codes VALUES(?, ?, ?, ?, ?, ?, ?, ?);
	
	--INSERT INTO "tab_ControlPoints" VALUES(1,'P.1','','catalog','CSP_TURKEY_ITRF$ZNA_Z1_27DEG','$',4262902.15,561563.72,125.11,127,0,0,NULL,'','',0);

	--INSERT INTO "tab_CoordinateSystems" VALUES('CSG_WGS_84$','CSE_WGS_84$','WGS 84',1,'',0,'GRP_WORLD','','',NULL,'','ELL_WGS_84',6378137,298.257223563,'','',0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL);
	--INSERT INTO "tab_CoordinateSystems" VALUES('CSL_LOCAL$','','Local',3,'',0,'GRP_WORLD','','',NULL,'','',0,1,'','',0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL);
	--INSERT INTO "tab_CoordinateSystems" VALUES('CSP_TURKEY_ITRF$ZNA_Z1_27DEG','CSG_ETRS89$','TR-ITRF 27-3-ITRF',2,'',0,'GRP_TURKEY_ITRF','','Turkey system on ETRS instead of ED50',NULL,'','ELL_GRS_1980',6378137,0.0033528107,'PRJ_Z1_27DEG','MPR_TRANSVERSE_MERCATOR',500000,0,0,0.471238898038,1,0,0,0,0,0,NULL,NULL,NULL,NULL);

	--INSERT INTO "tab_FeatureValues" VALUES(1,1,'','','');
	--INSERT INTO "tab_FeatureValues" VALUES(2,1,'','','');
	--INSERT INTO "tab_FeatureValues" VALUES(3,1,'','','');
	--INSERT INTO "tab_FeatureValues" VALUES(4,1,'','','');
	--INSERT INTO "tab_FeatureValues" VALUES(5,1,'','','');
	--INSERT INTO "tab_FeatureValues" VALUES(6,1,'YOL','','');
	--INSERT INTO "tab_FeatureValues" VALUES(7,1,'YOL','','');
	--INSERT INTO "tab_FeatureValues" VALUES(8,1,'YOL','','');
	
	--INSERT INTO "tab_Localizations" VALUES(1,1,'CSG_WGS_84$',NULL,'CSL_LOCAL$',NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,'$','','','');
	
	--INSERT INTO "tab_RTKBases" VALUES(1,1,'base_P.1','CSG_WGS_84$','$',38.49700478804,27.705707490706,125.109959467081,0,'autopos_1','CSG_WGS_84$','$',0,0,0,'JAV_TRIUMPH-1   NONE',1.365,1,NULL,NULL,1);
	--INSERT INTO "tab_RTKBases" VALUES(2,1,'RTCM3Net(212.156.70.42:2101)','CSG_WGS_84$','$',38.497004789012,27.705707494433,126.510256998241,0,'','$','$',0,0,0,'000',1.4,0,NULL,NULL,2);
	
	
	--INSERT INTO "tab_Representations" VALUES(1,1,'C',65280,2,1,'',1,65280,5);
	--INSERT INTO "tab_Representations" VALUES(2,1,'T',33023,2,1,'',1,33023,5);
	--INSERT INTO "tab_Representations" VALUES(3,1,'S',16711680,2,1,'',1,16711680,5);
	--INSERT INTO "tab_Representations" VALUES(5,1,'T',255,2,1,'',1,255,0);
	--INSERT INTO "tab_Representations" VALUES(6,1,'C',0,1,1,'',1,0,0);
	--INSERT INTO "tab_Representations" VALUES(7,1,'C',0,1,1,'',1,0,0);
	--INSERT INTO "tab_Representations" VALUES(8,1,'C',0,1,1,'',1,0,0);
	--INSERT INTO "tab_Representations" VALUES(9,1,'C',0,1,1,'',1,0,0);
	--INSERT INTO "tab_Representations" VALUES(13,1,'C',0,1,1,'',1,0,0);
	--INSERT INTO "tab_Representations" VALUES(15,1,'C',33023,2,5,'',1,33023,0);
	--INSERT INTO "tab_Representations" VALUES(16,1,'C',8421504,2,0,'',1,8421504,0);
	--INSERT INTO "tab_Representations" VALUES(17,1,'C',255,1,1,'',1,255,0);
	--INSERT INTO "tab_Representations" VALUES(18,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(19,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(20,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(21,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(22,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(23,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(24,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(25,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(26,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(27,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(28,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(29,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(30,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(31,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(32,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(33,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(34,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(35,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(36,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(37,1,'C',65280,2,3,'',2,0,0);
	--INSERT INTO "tab_Representations" VALUES(38,1,'C',65280,2,3,'',2,0,0);
	
	
	
	--INSERT INTO "tab_Settings" VALUES('CS_MEASURE_UNITS','0');
	--INSERT INTO "tab_Settings" VALUES('CS_MEASURE_UNITS_FEET_FORMAT','0');
	--INSERT INTO "tab_Settings" VALUES('CS_MEASURE_UNITS_LATLON_FORMAT','0');
	--INSERT INTO "tab_Settings" VALUES('CS_COORDINATE_ORDER','0');
	--INSERT INTO "tab_Settings" VALUES('CS_INVERTED_LOCAL_AXIS','0');
	--INSERT INTO "tab_Settings" VALUES('CS_Localization','1');
	--INSERT INTO "tab_Settings" VALUES('Catalog','Default Catalog');
	--INSERT INTO "tab_Settings" VALUES('Job_Name','YUSUF');
	--INSERT INTO "tab_Settings" VALUES('Job_Description','');
	--INSERT INTO "tab_Settings" VALUES('Job_Surveyer','');
	--INSERT INTO "tab_Settings" VALUES('Job_Agency','');
	--INSERT INTO "tab_Settings" VALUES('Job_UserInfo','');
	--INSERT INTO "tab_Settings" VALUES('CS_GEOID','$');
	--INSERT INTO "tab_Settings" VALUES('CS_CWS','CSP_TURKEY_ITRF$ZNA_Z1_27DEG');
	--INSERT INTO "tab_Settings" VALUES('CS_CVS','$');
	--INSERT INTO "tab_Settings" VALUES('ReceiverStyle_Style','');
	--INSERT INTO "tab_Settings" VALUES('SurveyStyle_Style','');
	--INSERT INTO "tab_Settings" VALUES('EquipmentInfo_ReceiverName','TRIUMPH1');
	--INSERT INTO "tab_Settings" VALUES('EquipmentInfo_ReceiverSN','03342');
	--INSERT INTO "tab_Settings" VALUES('EquipmentInfo_AntennaType','JAV_TRIUMPH-1   NONE');
	--INSERT INTO "tab_Settings" VALUES('EquipmentInfo_ControllerName','Javad Victor');
	--INSERT INTO "tab_Settings" VALUES('EquipmentInfo_ControllerSN','95800');
	--INSERT INTO "tab_Settings" VALUES('CurCode','YOL');
	
	
	
	
	--INSERT INTO "tab_SurveyInfos" VALUES(1,1,5,5,0,2.14622,0,0,0,0,0,0,873,558483000,181948628,181948628,0,'JAV_TRIUMPH-1   NONE',1.365,1,1.455486,0,1.897406,1.003052,0,0,0,0.182);
	--INSERT INTO "tab_SurveyInfos" VALUES(2,0,0,0,0,0,0,0,0,0,0,0,0,0,182748776,182748776,0,'000',1.4,0,0,0,0,0,0,0,0,0);
	--INSERT INTO "tab_SurveyInfos" VALUES(3,4,4,4,0,2.538329,0.000166,0.000069,0.000126,0.000081,0.000071,0.00025,873,559285000,182747470,182750028,3,'JAV_TRIUMPH-1   NONE',2.4,0,1.97112,3.213787,2.251879,1.17139,0,0,0,0.182);
	--INSERT INTO "tab_SurveyInfos" VALUES(4,4,5,4,0,2.061169,0.000434,0.000033,0.000076,0.000045,0.00002,0.00025,873,559294000,182756797,182759029,3,'JAV_TRIUMPH-1   NONE',2.4,0,1.484798,2.540284,1.738134,1.107839,0,0,0,0.182);
	--INSERT INTO "tab_SurveyInfos" VALUES(5,4,4,4,0,2.915758,0.000438,0.000055,0.000139,0.000051,0.000013,0.000285,873,559303000,182765579,182768008,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.139878,3.616728,2.411478,1.639029,0,0,0,0.182);
	--INSERT INTO "tab_SurveyInfos" VALUES(6,4,4,3,0,3.612684,0.000429,0.000078,0.000043,0.000069,-0.000047,0.000736,873,559311000,182773802,182776008,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.310262,4.288216,2.463675,2.642308,0,0,0,0.182);
	--INSERT INTO "tab_SurveyInfos" VALUES(7,4,4,3,0,3.611556,0.000381,0.000078,0.000043,0.000147,-0.000047,0.001294,873,559319000,182781729,182783959,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.311051,4.287691,2.463391,2.64103,0,0,0,0.182);
	--INSERT INTO "tab_SurveyInfos" VALUES(8,4,5,2,0,3.390082,0.000828,0.000047,0.000025,0.000221,-0.000081,0.000852,873,559342000,182803490,182806936,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.158293,4.018816,2.109888,2.653493,0,0,0,0.182);
	--INSERT INTO "tab_SurveyInfos" VALUES(9,4,5,2,0,3.385339,0.000323,0.000046,0.000025,0.000166,-0.00008,0.000693,873,559354000,182816778,182818740,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.157693,4.014494,2.110352,2.647062,0,0,0,0.182);
	--INSERT INTO "tab_SurveyInfos" VALUES(10,4,4,4,0,3.21548,0.000525,0.000109,0.000221,0.000313,0.000133,0.004118,873,559883000,183345353,183347782,3,'JAV_TRIUMPH-1   NONE',2.4,0,2.553061,4.10578,2.921841,1.342444,0,0,0,0.182);
	
	
	
	
	
	INSERT INTO "tab_SurveyPoints" VALUES(1,'1','','',NULL,NULL,'CSG_WGS_84$','$',NULL,38.4970025038,27.7056192026,124.373743,0.011571,0.019051,NULL,2,3,'',0);
	INSERT INTO "tab_SurveyPoints" VALUES(2,'2','','',NULL,NULL,'CSG_WGS_84$','$',NULL,38.497025652,27.7055679836,124.387604,0.011907,0.020068,NULL,2,4,'',0);
	INSERT INTO "tab_SurveyPoints" VALUES(3,'3','','',NULL,NULL,'CSG_WGS_84$','$',NULL,38.4970449775,27.7055262941,124.370856,0.013815,0.019663,NULL,2,5,'',0);
	INSERT INTO "tab_SurveyPoints" VALUES(4,'4','','',NULL,NULL,'CSG_WGS_84$','$',NULL,38.4970535768,27.7055063041,124.341817,0.022907,0.021437,NULL,2,6,'',0);
	INSERT INTO "tab_SurveyPoints" VALUES(5,'5','','',NULL,NULL,'CSG_WGS_84$','$',NULL,38.4970715481,27.7054659028,124.341558,0.032593,0.027268,NULL,2,7,'',0);
	INSERT INTO "tab_SurveyPoints" VALUES(6,'6','','YOL',NULL,NULL,'CSG_WGS_84$','$',NULL,38.497040883,27.7054507437,124.38405,0.022462,0.018013,NULL,2,8,'',0);
	INSERT INTO "tab_SurveyPoints" VALUES(7,'7','','YOL',NULL,NULL,'CSG_WGS_84$','$',NULL,38.4970184262,27.7055010808,124.510394,0.025135,0.020284,NULL,2,9,'',0);
	INSERT INTO "tab_SurveyPoints" VALUES(8,'8','','YOL',NULL,NULL,'CSG_WGS_84$','$',NULL,38.496986525,27.7056151127,118.283616,0.049432,0.045346,NULL,2,10,'',0);
	
	
	
	
	--INSERT INTO "tab_TransformationLists" VALUES(1,'CSG_WGS_84$','$','CSL_LOCAL$','$','<TRANSFORM_LIST SOURCE_COORDINATE_SYSTEM_NAME="WGS 84" SOURCE_COORDINATE_SYSTEM_ID="CSG_WGS_84" SOURCE_COORDINATE_SYSTEM_ZN="" SOURCE_VERTICAL_SYSTEM_ID="" SOURCE_VERTICAL_SYSTEM_ZN="" TARGET_COORDINATE_SYSTEM_NAME="Local engineering grid" TARGET_COORDINATE_SYSTEM_ID="CSL_LOCAL" TARGET_COORDINATE_SYSTEM_ZN="" TARGET_VERTICAL_SYSTEM_ID="" TARGET_VERTICAL_SYSTEM_ZN="" LANGUAGE_ID="en-US"> <TRANSFORM NAME="WGS to Local" selected="true"> <MDL_GEODETIC_LOCALIZATION  PAR_LATITUDE_OFFSET="0.000000000000000" PAR_LONGITUDE_OFFSET="0.000000000000000" PAR_ROTATION_ANGLE_OF_SOURCE_COORDINATE_REFERENCE_SYSTEM_AXES="0.000000000000000" PAR_SCALE_DIFFERENCE="0.000000000000000" PAR_ORDINATE_1_OF_EVALUATION_POINT="0.000000" PAR_ORDINATE_2_OF_EVALUATION_POINT="0.000000" PAR_VERTICAL_OFFSET="0.000000" PAR_INCLINATION_IN_NORTHING="0.000000000000000" PAR_INCLINATION_IN_EASTING="0.000000000000000" PAR_SEMI_MAJOR_AXIS_SOURCE="6378137.0" PAR_FLATTENING_SOURCE="0.0033528106647474807198455286185206" PAR_PRIME_MERIDIAN_GREENWICH_LONGITUDE_SOURCE="0" PAR_SEMI_MAJOR_AXIS_TARGET="-1" PAR_FLATTENING_TARGET="-1" PAR_PRIME_MERIDIAN_GREENWICH_LONGITUDE_TARGET="0" forward="true" /> </TRANSFORM> </TRANSFORM_LIST>','');
	--INSERT INTO "tab_TransformationLists" VALUES(2,'CSG_WGS_84$','$','CSP_TURKEY_ITRF$ZNA_Z1_27DEG','$','<?xml version="1.0" encoding="utf-8" standalone="yes"?> <TRANSFORM_LIST  	SOURCE_COORDINATE_SYSTEM_NAME="WGS84(ITRF2008)"  	SOURCE_COORDINATE_SYSTEM_ID="CSG_WGS_84"  	SOURCE_COORDINATE_SYSTEM_ZN=""  	SOURCE_VERTICAL_SYSTEM_ID=""  	SOURCE_VERTICAL_SYSTEM_ZN=""  	TARGET_COORDINATE_SYSTEM_NAME="TR-ITRF / 27-3-ITRF"  	TARGET_COORDINATE_SYSTEM_ID="CSP_TURKEY_ITRF"  	TARGET_COORDINATE_SYSTEM_ZN="ZNA_Z1_27DEG"  	TARGET_VERTICAL_SYSTEM_ID=""  	TARGET_VERTICAL_SYSTEM_ZN=""  	SOUTH_BOUND_LAT="0.63704517697793"  	NORTH_BOUND_LAT="0.734783615089613"  	WEST_BOUND_LON="0.446629755585349"  	EAST_BOUND_LON="0.497418836818384"  	EPOCH="2016.38524590164"  	LANGUAGE_ID="en-US"  > 	<TRANSFORM NAME="ETRS89 to WGS 84 (1) / EPSG-eur / 3-degree TM zone 1"  	SOUTH_BOUND_LAT="0.602138591938044"  	NORTH_BOUND_LAT="1.24005643354197"  	WEST_BOUND_LON="0.445058959258554"  	EAST_BOUND_LON="0.497418836818384"  	selected="true"> 		<MDL_POSITION_VECTOR_7_PARAM_TRANSFORMATION PAR_FLATTENING_SOURCE="0.00335281068118232" PAR_FLATTENING_TARGET="0.00335281066474748" PAR_PRIME_MERIDIAN_GREENWICH_LONGITUDE_SOURCE="0" PAR_PRIME_MERIDIAN_GREENWICH_LONGITUDE_TARGET="0" PAR_SCALE_DIFFERENCE="0" PAR_SEMI_MAJOR_AXIS_SOURCE="6378137" PAR_SEMI_MAJOR_AXIS_TARGET="6378137" PAR_X_AXIS_ROTATION="0" PAR_X_AXIS_TRANSLATION="0" PAR_Y_AXIS_ROTATION="0" PAR_Y_AXIS_TRANSLATION="0" PAR_Z_AXIS_ROTATION="0" PAR_Z_AXIS_TRANSLATION="0" forward="false"/> 		<MPR_TRANSVERSE_MERCATOR PAR_FALSE_EASTING="500000" PAR_FALSE_NORTHING="0" PAR_FLATTENING="0.00335281068118232" PAR_LATITUDE_OF_NATURAL_ORIGIN="0" PAR_LONGITUDE_OF_NATURAL_ORIGIN="0.47123889803846" PAR_PRIME_MERIDIAN_GREENWICH_LONGITUDE="0" PAR_SCALE_FACTOR_AT_NATURAL_ORIGIN="1" PAR_SEMI_MAJOR_AXIS="6378137" forward="true"/> 	</TRANSFORM> </TRANSFORM_LIST> ','');
	
	
	
	
	--INSERT INTO "tab_Versions" VALUES(1,'tab_Versions',1,0);
	--INSERT INTO "tab_Versions" VALUES(2,'tab_Settings',1,0);
	--INSERT INTO "tab_Versions" VALUES(3,'tab_ControlPoints',1,0);
	--INSERT INTO "tab_Versions" VALUES(4,'tab_SurveyPoints',1,0);
	--INSERT INTO "tab_Versions" VALUES(5,'tab_DesignPoints',1,0);
	--INSERT INTO "tab_Versions" VALUES(6,'tab_PointsToSurvey',1,0);
	--INSERT INTO "tab_Versions" VALUES(7,'tab_BackgroundMapPoints',1,0);
	--INSERT INTO "tab_Versions" VALUES(8,'tab_BackgroundImages',1,0);
	--INSERT INTO "tab_Versions" VALUES(9,'tab_BackgroundLayers',1,0);
	--INSERT INTO "tab_Versions" VALUES(10,'tab_Codes',1,0);
	--INSERT INTO "tab_Versions" VALUES(11,'tab_Features',1,0);
	--INSERT INTO "tab_Versions" VALUES(12,'tab_Domains',1,0);
	--INSERT INTO "tab_Versions" VALUES(13,'tab_ValueLists',1,0);
	--INSERT INTO "tab_Versions" VALUES(14,'tab_FeatureValues',1,0);
	--INSERT INTO "tab_Versions" VALUES(15,'tab_CodeGroups',1,0);
	--INSERT INTO "tab_Versions" VALUES(16,'tab_CoordinateSystems',1,0);
	--INSERT INTO "tab_Versions" VALUES(17,'tab_Localizations',1,0);
	--INSERT INTO "tab_Versions" VALUES(18,'tab_IdenticalPoints',1,0);
	--INSERT INTO "tab_Versions" VALUES(19,'tab_IsolinePoints',1,0);
	--INSERT INTO "tab_Versions" VALUES(20,'tab_Datums',1,0);
	--INSERT INTO "tab_Versions" VALUES(21,'tab_TransformationLists',1,0);
	--INSERT INTO "tab_Versions" VALUES(22,'tab_PointLists',1,0);
	--INSERT INTO "tab_Versions" VALUES(23,'tab_RTKBases',1,0);
	--INSERT INTO "tab_Versions" VALUES(24,'tab_SurveyInfos',1,0);
	--INSERT INTO "tab_Versions" VALUES(25,'tab_RawDatas',1,0);
	--INSERT INTO "tab_Versions" VALUES(26,'tab_Stakeouts',1,0);
	--INSERT INTO "tab_Versions" VALUES(27,'tab_Antennas',1,0);
	--INSERT INTO "tab_Versions" VALUES(28,'tab_Selections',1,0);
	--INSERT INTO "tab_Versions" VALUES(29,'tab_JobHistory',1,0);
	--INSERT INTO "tab_Versions" VALUES(30,'tab_Representations',2,0);
	--INSERT INTO "tab_Versions" VALUES(31,'tab_ComplexObjectParts',1,0);
	--INSERT INTO "tab_Versions" VALUES(32,'tab_ComplexObjects',1,0);
	--INSERT INTO "tab_Versions" VALUES(33,'tab_RoadsEx',1,0);
	--INSERT INTO "tab_Versions" VALUES(34,'tab_RoadStartPointsEx',1,0);
	--INSERT INTO "tab_Versions" VALUES(35,'tab_RoadHAlignLinesEx',1,0);
	--INSERT INTO "tab_Versions" VALUES(36,'tab_RoadHAlignArcsEx',1,0);
	--INSERT INTO "tab_Versions" VALUES(37,'tab_RoadHAlignSpiralsEx',1,0);
	--INSERT INTO "tab_Versions" VALUES(38,'tab_RoadVAlignGradesEx',1,0);
	--INSERT INTO "tab_Versions" VALUES(39,'tab_RoadVAlignParabolsEx',1,0);
	--INSERT INTO "tab_Versions" VALUES(40,'tab_RoadXSections',1,0);
	--INSERT INTO "tab_Versions" VALUES(41,'tab_RoadXSectionTemplates',1,0);
	--INSERT INTO "tab_Versions" VALUES(42,'tab_RoadXSectionSegments',1,0);
	--INSERT INTO "tab_Versions" VALUES(43,'tab_MediaFiles',1,0);
	--INSERT INTO "tab_Versions" VALUES(44,'tab_TotalStationPoints',1,0);
	--INSERT INTO "tab_Versions" VALUES(45,'tab_TotalStationMeasurements',1,0);
	--INSERT INTO "tab_Versions" VALUES(46,'tab_CoordinatesCWS',1,0);
	--INSERT INTO "tab_Versions" VALUES(47,'DataBase',50,0);
	
	*/
	
}
